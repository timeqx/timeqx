/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDate>
#include <QSvgRenderer>
#include <QPixmap>
#include <QIcon>
#include <QFile>
#include <QPainter>

#include "repository/user.h"
#include "timemilagetracker/timemilagewidget.h"
#include "timemilagetracker/timemilagemodel.h"

#include "weektimesummarydialog/weektimesummarydialog.h"


#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mUser(nullptr)
{
    ui->setupUi(this);

    // Connect to the application.
    QCoreApplication* application = QCoreApplication::instance();
    connect(application, &QCoreApplication::aboutToQuit, this, &MainWindow::aboutToQuit);
    connect(ui->actionAvsluta, &QAction::triggered, application, &QCoreApplication::quit);

    // Set the application-icon
    setWindowIcon(QIcon(":/artwork/timeqx-icon.svg"));

    connect(ui->actionSpara, &QAction::triggered, ui->pTimeMilageWidget, &TimeMilageWidget::save);
    connect(this, &MainWindow::aboutToQuit, ui->pTimeMilageWidget, &TimeMilageWidget::save);

    connect(ui->actionShowWeekTimeSummary, &QAction::triggered, this, &MainWindow::showTimeSummaryDialog);



    QString     basePath;
    QString     user;
    bool        nextIsBasePath  = false;
    bool        nextIsUser      = false;


    QStringList args = QApplication::arguments();
    qDebug() << "Command-line: " << args;

    for(QStringList::Iterator i = args.begin(); i != args.constEnd(); i++)
    {
        QString arg=(*i);

        if (nextIsBasePath)
        {
            basePath = arg;
            nextIsBasePath = false;
        }
        else if (nextIsUser)
        {
            user = arg;
            nextIsUser = false;
        }
        else if (arg == "-d")
            nextIsBasePath = true;
        else if (arg == "-u")
            nextIsUser = true;

    }

    if (user.isNull() || basePath.isNull())
    {
        qDebug() << "Failed to read user and/or base-path. User -d <path> -u <user> as command args";
        QApplication::exit(1);
    }
    qDebug() << "Extracted user " << user << " and basePath " << basePath << " from commandline. Lets read the files, shall we?";

    mUser = new User(user);

    connect(this, &MainWindow::save, mUser, &User::doSave);

    mUser->readFromDisk(basePath);

    ui->pTimeMilageWidget->setProfile(mUser);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showTimeSummaryDialog()
{
    if (!mTimeSummaryDialog)
    {
        mTimeSummaryDialog = new WeekTimeSummaryDialog(this);
        mTimeSummaryDialog->setProfile(mUser);
    }

    mTimeSummaryDialog->show();

}
