#include "tmttotalsummary.h"

#include "tmttimesummary.h"

TMTTotalSummary::TMTTotalSummary():
    QStandardItem ("Total tid"),
    mTimeSummary(new TMTTimeSummary)
{
    QFont   font  = this->font();
    QBrush  brush = this->foreground();

    font.setItalic(true);
    font.setBold(true);
    brush.setColor(Qt::darkGray);

    this->setFont(font);
    this->setForeground(brush);

    mTimeSummary->setFont(font);
    mTimeSummary->setForeground(brush);

}

QList<QStandardItem *> TMTTotalSummary::row()
{
    QList<QStandardItem*> result;

    result.append(this);
    result.append(mTimeSummary);
    result.append(new QStandardItem);

    return result;

}

TMTTimeSummary *TMTTotalSummary::totalTimeSummary()
{
    return mTimeSummary;
}
