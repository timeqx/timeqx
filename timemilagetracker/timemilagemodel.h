/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TIMEMILAGEMODEL_H
#define TIMEMILAGEMODEL_H

#include <QObject>
#include <QPointer>
#include <QStandardItemModel>

#include "repository/user.h"
#include "repository/julianday.h"

class TMTActivity;
class TMTTotalSummary;

class TimeMilageModel : public QStandardItemModel
{
        Q_OBJECT
    public:
        explicit TimeMilageModel(User* user, JulianDay startDay);

        TMTActivity *firstActivity();

    signals:
        void currentDayChanged(JulianDay day);
        void itemChanged(QStandardItem *item);
        void timeUpdated();



    public slots:

        void setCurrentDay(JulianDay day);
        void addContext(Context* context);



    private:

        TMTTotalSummary*    mTotalSummary;
        JulianDay           mCurrentDay;
        QPointer<User>      mUser;
};

#endif // TIMEMILAGEMODEL_H
