/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "tmttimer.h"
#include "tmtactivity.h"

#include "repository/activity.h"
#include "repository/timemilagerecord.h"

#include <QtDebug>

// For debuging purposes, change this to test day-shifts.
static int  julianOffest = 0;

// Timers in miliseconds
static int  updateTimerInterval = 10000;    // Set the updateTimer
static bool bAdhereToTimer      = true;     // Adhere to the timer
static bool bAdhereToTimerOnce  = false;    // Adhere to the timer just one time.

static int saveTimerInteral     = 300000;   // Save every 5 minutes.

TMTTimer::TMTTimer() :
    QObject(nullptr),
    mActive(false),
    mPaused(true)
{
    connect(&mUpdateTimer, &QTimer::timeout, this, &TMTTimer::updateTime);
	connect(&mUpdateTimer, &QTimer::timeout, this, &TMTTimer::detectPausedDateChange);
    connect(&mSaveTimer, &QTimer::timeout, this, &TMTTimer::timeToSave);

}

bool TMTTimer::isActive() const
{
    return mActive;
}

bool TMTTimer::isPaused() const
{
    return mPaused;
}

JulianDay TMTTimer::currentDay() const
{

    return currentDateTime().date().toJulianDay();
}

void TMTTimer::setActive(bool active)
{
    mActive = active;
}

void TMTTimer::setPaused(bool paused)
{

    if (paused)
    {
        pauseTimer();
        updateTime();

        emit timeToSave();
    }
    else
    {
        activitySelected(mCurrentActivity);
        emit resumed(mCurrentActivity);
    }

}

void TMTTimer::activitySelected(TMTActivity *activity)
{
    if (activity)
    {
        mCurrentActivity = activity;
        mActive = true;
        mOnSince = currentDateTime();
        startTimer();
    }
}

void TMTTimer::detectPausedDateChange()
{
	// In paused mode this will trigger a day-shift that will otherwise be missed.

    if (isPaused())
    {
        // While in paused mode we just need to detect the
        // change and set a new mOnSince, no need to add time
        // or so. Just change.
        if (mOnSince.date() != currentDateTime().date())
        {
            mOnSince = currentDateTime();
            emit currentDayChanged(currentDateTime().date().toJulianDay());
            emit timeUpdated();
        }

    }

}

void TMTTimer::updateTime()
{
    // For debuging, allows to chose to ignore or adhere to the timer from the debugger.
    if (!bAdhereToTimer && !bAdhereToTimerOnce)
        return;

    if (bAdhereToTimerOnce)
        bAdhereToTimerOnce = false;

	if (!mCurrentActivity)
		return;

    QDate currentDate = currentDateTime().date();
    if (mOnSince.date() == currentDate)
    {
        Activity* activity = mCurrentActivity->activity();
        TimeMilageRecord* record = activity->record(currentDateTime().date().toJulianDay(), true);
        record->addTime(mOnSince.time().secsTo(QTime::currentTime()));
        mOnSince = currentDateTime();
    }

    else if (mOnSince.date() < currentDate)
    {
        QTime   beginingOfTheDay(0,0,0,000);
        QTime   endOfTheDay(23,59,59,999);

        // Add the part of the first day that is still missing.
        Activity*           activity = mCurrentActivity->activity();
        TimeMilageRecord*   record  = activity->record(mOnSince.date().toJulianDay(), true);
        int                 delta   = mOnSince.time().secsTo(endOfTheDay);
        record->addTime(delta);

        // Add the midle parts
        for (JulianDay i = mOnSince.date().toJulianDay() + 1; i < currentDateTime().date().toJulianDay(); i++)
        {

            emit currentDayChanged(i);

            // Here we have whole days
            record = activity->record(i, true);
            record->addTime(beginingOfTheDay.secsTo(endOfTheDay));

        }

        // Ad today - the last part.
        emit currentDayChanged(currentDay());
        record = activity->record(currentDay(), true);
        record->addTime(beginingOfTheDay.secsTo(currentDateTime().time()));
        mOnSince = currentDateTime();

    }
    else
        qDebug() << "Unexpected activated and present timestamps";

    emit timeUpdated();

}

void TMTTimer::startTimer()
{
    // Some extras to be able to debug properly. Low cost, no need to deal with ifdefs.
    // bStartTimer is defaulted to true but is an easy way to turn of the updateTimer.
    // So is the intervall.

    mUpdateTimer.start(updateTimerInterval);

    if (!mSaveTimer.isActive())
        mSaveTimer.start(saveTimerInteral);
}

void TMTTimer::pauseTimer()
{
    // We leave the save-timer ongpoing also in paused mode to detect
    // date-changes and to save things - time can be adjusted by the user.
    mUpdateTimer.stop();
//    mSaveTimer.stop();

}

QDateTime TMTTimer::currentDateTime() const
{
    // Add the static julian-ofset to be able to make proper debug of day-shifts.
    return QDateTime::currentDateTime().addDays(julianOffest);
}

