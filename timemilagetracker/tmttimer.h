/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TMTTIMER_H
#define TMTTIMER_H

#include <QObject>
#include <QPointer>
#include <QTime>
#include <QDateTime>
#include <QTimer>

#include "repository/julianday.h"

class TMTActivity;

class TMTTimer : public QObject
{
        Q_OBJECT
    public:
        explicit TMTTimer();
        bool isActive() const;
        bool isPaused() const;

        JulianDay   currentDay() const;


    signals:

        void currentDayChanged(JulianDay day);
        void timeUpdated();

        void timeToSave();

        void resumed(TMTActivity* resumedActivity);

    public slots:


        void setActive(bool active = true);
        void setPaused(bool paused = true);

        void activitySelected(TMTActivity* activity);

    protected slots:
        void detectPausedDateChange();
        void updateTime();
        void startTimer();
        void pauseTimer();

    protected:

        QDateTime   currentDateTime() const;


    private:

        bool        mActive;
        bool        mPaused;

        QPointer<TMTActivity>       mCurrentActivity;
        QDateTime                   mOnSince;
        QTimer                      mUpdateTimer;
        QTimer                      mSaveTimer;

};


#endif // TMTTIMER_H
