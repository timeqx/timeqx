#include "tmttimesummary.h"

#include "tmtactivity.h"
#include "tmttimeentry.h"

#include <QTime>

#include <QTimer>
#include <QtDebug>


TMTTimeSummary::TMTTimeSummary():
    QObject(nullptr),
    mTotalSeconds(0)
{
    QBrush  brush = this->foreground();
    brush.setColor(Qt::darkGray);
    this->setForeground(brush);

    QFont   font  = this->font();
    font.setItalic(true);
    this->setFont(font);


    updateVisualTime();

}

void TMTTimeSummary::track(TMTTimeEntry *entry)
{



    if (entry != nullptr && !mEntries.contains(entry))
    {
        connect(entry, &TMTTimeEntry::itemChanged, this, &TMTTimeSummary::updateSummary);

        mEntries.append(entry);
        updateSummary();
        emit itemTracked(entry);
    }

}

void TMTTimeSummary::trackSame(TMTTimeSummary *other)
{
    foreach(TMTTimeEntry* entry, other->mEntries)
    {
        if (entry != nullptr && !mEntries.contains(entry))
        {
            connect(entry, &TMTTimeEntry::itemChanged, this, &TMTTimeSummary::updateSummary);

            mEntries.append(entry);
            updateSummary();
        }
    }

}

void TMTTimeSummary::follow(TMTTimeSummary *other)
{
    if (!other)
        return;

    connect(other, &TMTTimeSummary::itemTracked, this, &TMTTimeSummary::track);

}

void TMTTimeSummary::updateSummary(QStandardItem *item)
{
    Q_UNUSED(item)

    QTime zeroTime(0,0,0,0);
    int newTotalTime = 0;
    foreach(TMTTimeEntry* entry, mEntries)
    {
        newTotalTime += zeroTime.secsTo(entry->time());
    }

    mTotalSeconds = newTotalTime;

    updateVisualTime();
    emit timeChanged(QTime(0,0,0,0).addSecs(mTotalSeconds));

}

void TMTTimeSummary::updateVisualTime()
{
    // We may have more than 24 hours here so we can't use the QTime as formatter.
    int hours = mTotalSeconds / (60 * 60);
    int minutes = mTotalSeconds % (60 * 60) / 60;
//    int seconds = mTotalSeconds % 60;

    QString timeString;
    if (mTotalSeconds > 1)
    {
        // Seconds are very usefull dusring development.
//        QString secondsPart = QString("00%1").arg(seconds).right(2);
        QString minutePart = QString("00%1").arg(minutes).right(2);
        timeString = QString("%1:%2").arg(hours).arg(minutePart); // .arg(secondsPart);
    }
    else
        timeString = "  ";


    setText(timeString);
    emit itemChanged(this);


}
