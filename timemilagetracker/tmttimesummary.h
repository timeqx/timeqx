#ifndef TMTTIMESUMMARY_H
#define TMTTIMESUMMARY_H

#include <QObject>
#include <QStandardItem>

#include "repository/julianday.h"

class TMTTimeEntry;
class TMTActivity;

class TMTTimeSummary : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit TMTTimeSummary();

        void trackSame(TMTTimeSummary* other);

        void follow(TMTTimeSummary* other);

    signals:
        void itemChanged(QStandardItem *item);
        void timeChanged(const QTime& newTime);
        void itemTracked(TMTTimeEntry* entry);

    public slots:
        void updateSummary(QStandardItem *item = nullptr);
        void track(TMTTimeEntry* entry);


    protected:
        void updateVisualTime();

    private:
        QList<TMTTimeEntry*> mEntries;

        int     mTotalSeconds;
};

#endif // TMTTIMESUMMARY_H
