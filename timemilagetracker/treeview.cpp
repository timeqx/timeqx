/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "timemilagemodel.h"
#include "tmttimeentry.h"
#include "treeview.h"

#include <QWheelEvent>
#include <QModelIndex>

TreeView::TreeView(QWidget *parent):
    QTreeView (parent)
{

}

void TreeView::wheelEvent(QWheelEvent *event)
{
    // One event is expected to be sent every step the wheel takes.

    QModelIndex index = indexAt(event->pos());

    // The delta is 2-dimensional and the event can be a side-scroll that we ignores.
    int steps = 0;

    if (event->angleDelta().y() > 0)
        steps = 1;
    else if (event->angleDelta().y() < 0)
        steps = -1;
    else
    {
        event->ignore();
        return;
    }

    QStandardItemModel* model = dynamic_cast<QStandardItemModel*>(this->model());

    TMTTimeEntry* entry = dynamic_cast<TMTTimeEntry*>(model->itemFromIndex(index));
    if (!entry)
    {
        event->ignore();
        return;
    }

    // Adjust minutes
    if (event->modifiers() == Qt::NoModifier)
    {
        if (steps > 0)
            entry->increaseMinute();
        else
            entry->decreaseMinute();

        event->accept();

    }
    // Adjust Hours
    else if (event->modifiers() == Qt::ShiftModifier)
    {
        if (steps > 0)
            entry->increaseHour();
        else
            entry->decreaseHour();

        event->accept();

    }
    // Move Time
    else if (event->modifiers() == Qt::ControlModifier || event->modifiers() == (Qt::ControlModifier | Qt::ShiftModifier))
    {

        QModelIndex  currentIndex = this->currentIndex().siblingAtColumn(1);


        TMTTimeEntry* currentEntry = dynamic_cast<TMTTimeEntry*>(model->itemFromIndex(currentIndex));
        if (!currentEntry)
        {
            event->ignore();
            return;
        }


        if (event->modifiers() & Qt::ShiftModifier)
        {
            if (steps > 0)
            {
                int change = currentEntry->decreaseHour();
                entry->addTime(-1 * change);
            }
            else
            {
                int change = entry->decreaseHour();
                currentEntry->addTime(-1 * change);
            }
            event->accept();
        }
        else
        {
            if (steps > 0)
            {
                int change = currentEntry->decreaseMinute();
                entry->addTime(-1 * change);
            }
            else
            {
                int change = entry->decreaseMinute();
                currentEntry->addTime(-1 * change);
            }
            event->accept();
        }

    }

    else
        event->ignore();

}

//void TreeView::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
//{
////    QTreeView::selectionChanged(selected, deselected);
////    QModelIndexList selctions = selected.indexes();


//}
