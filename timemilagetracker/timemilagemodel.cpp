/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "timemilagemodel.h"
#include "tmtactivity.h"
#include "tmttotalsummary.h"

#include "repository/context.h"
#include "tmtcontext.h"

TimeMilageModel::TimeMilageModel(User* user, JulianDay startDay) :
    mTotalSummary(new TMTTotalSummary),
    mCurrentDay(startDay),
    mUser(user)
{
    QStringList hHeader;

    if (!mUser)
        return;

    connect(mUser, &User::contextAdded, this, &TimeMilageModel::addContext);

    setColumnCount(3);

    hHeader << "Aktivitet" << "Tid" << "Körning";
    setHorizontalHeaderLabels(hHeader);

    // Add the total summary as the first row.

    appendRow(mTotalSummary->row());

    if (mUser)
    {
        foreach(QString contextName, mUser->contextNames())
        {
            Context *context = mUser->context(contextName, false);
            addContext(context);

        }
    }

    setCurrentDay(startDay);

}

TMTActivity *TimeMilageModel::firstActivity()
{
    for (int i = 0; i < rowCount(); i++)
    {
        TMTContext* context = dynamic_cast<TMTContext*>(item(i));

        if (context)
        {
            for (int j = 0; j < context->rowCount(); j++)
            {
                TMTActivity* activity = dynamic_cast<TMTActivity*>(context->child(j));
                if (activity)
                {
                    return activity;
                }
            }

        }
    }

    return nullptr;
}


void TimeMilageModel::setCurrentDay(JulianDay day)
{
    if (mUser)
        mUser->trackingRecord(day);

    emit currentDayChanged(day);


}

void TimeMilageModel::addContext(Context *context)
{
    if (context)
    {
        TMTContext* tmtContext = new TMTContext(mTotalSummary->totalTimeSummary(), context);
        appendRow(tmtContext->row());

        connect(this, &TimeMilageModel::currentDayChanged, tmtContext, &TMTContext::setCurrentDay);
        connect(tmtContext, &TMTContext::itemChanged, this, &TimeMilageModel::itemChanged);
        connect(this, &TimeMilageModel::timeUpdated, tmtContext, &TMTContext::timeUpdated);
    }

}
