#ifndef TMTTOTALSUMMARY_H
#define TMTTOTALSUMMARY_H

#include <QObject>
#include <QStandardItem>


class TMTTimeSummary;

class TMTTotalSummary : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit TMTTotalSummary();

        QList<QStandardItem*> row();

        TMTTimeSummary* totalTimeSummary();

    signals:
        void itemChanged(QStandardItem *item);

    public slots:

    private:

        TMTTimeSummary*     mTimeSummary;
};

#endif // TMTTOTALSUMMARY_H
