/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TMTTIMEENTRY_H
#define TMTTIMEENTRY_H

#include <QObject>
#include <QPointer>
#include <QStandardItem>

#include <repository/julianday.h>

class TimeMilageRecord;
class Activity;

class TMTTimeEntry : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit TMTTimeEntry(Activity* source);

        int increaseMinute();
        int increaseHour();
        int decreaseMinute();
        int decreaseHour();
        int addTime(int time);
        QTime time() const;



    signals:
        void currentDayChanged(JulianDay day);
        void itemChanged(QStandardItem *item);


    public slots:

        void setCurrentDay(JulianDay day);
        void sourceTimeChanged(const QTime& newTime);

    protected:
        QString formatTime(const QTime& time) const;
        QTime   parseTime(const QString& input) const;
    private:

        QPointer<Activity>          mSource;
        QPointer<TimeMilageRecord>  mRecord;
        int mObjectNr;


};

#endif // TMTTIMEENTRY_H
