/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "tmttimeentry.h"

#include "repository/activity.h"
#include "repository/timemilagerecord.h"

static int objectNr = 0;

TMTTimeEntry::TMTTimeEntry(Activity *source):
    QObject(nullptr),
    QStandardItem ("--:--"),
    mSource(source),
    mObjectNr(objectNr++)
{

//    if (mSource)
    //        setText(mSource->hhmmTime());
}

int TMTTimeEntry::increaseMinute()
{
    if (mRecord)
        return mRecord->addTime(60);

    return 0;
}

int TMTTimeEntry::increaseHour()
{
    if (mRecord)
        return mRecord->addTime(60*60);

    return 0;
}

int TMTTimeEntry::decreaseMinute()
{
    if (mRecord)
        return mRecord->addTime(-60);

    return 0;
}

int TMTTimeEntry::decreaseHour()
{
    if (mRecord)
        return mRecord->addTime(-60*60);

    return 0;

}

int TMTTimeEntry::addTime(int time)
{

    if (mRecord)
        return mRecord->addTime(time);

    return 0;

}

QTime TMTTimeEntry::time() const
{
    if (mRecord)
        return mRecord->time();

    return QTime(0,0,0,0);

}



void TMTTimeEntry::setCurrentDay(JulianDay day)
{
    if (mRecord)
        disconnect(mRecord, &TimeMilageRecord::timeChanged, this, &TMTTimeEntry::sourceTimeChanged);


    mRecord = mSource->record(day, true);
    if (mRecord)
    {
        sourceTimeChanged(mRecord->time());
        connect(mRecord, &TimeMilageRecord::timeChanged, this, &TMTTimeEntry::sourceTimeChanged);

    }

    emit currentDayChanged(day);

}

void TMTTimeEntry::sourceTimeChanged(const QTime &newTime)
{
    if (newTime > QTime(0,0,1,0))
        setText(formatTime(newTime));
    else
        setText("   ");

    emit itemChanged(this);

}

QString TMTTimeEntry::formatTime(const QTime &time) const
{
    return time.toString("H:mm");
}

QTime TMTTimeEntry::parseTime(const QString &input) const
{

    QTime result = QTime::fromString(input, "H:mm");

    if (!result.isValid())
        result = QTime::fromString(input, "HH:mm");

    if (!result.isValid())
        result = QTime::fromString(input, "H:mm:ss");

    if (!result.isValid())
        result = QTime::fromString(input, "HH:mm:ss");



    return result;
}
