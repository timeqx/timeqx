/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "timemilagewidget.h"
#include "ui_timemilagewidget.h"

#include "repository/user.h"
#include "repository/activity.h"

#include "tmtactivity.h"

#include <QPushButton>
#include <QFont>

#include <QtDebug>

TimeMilageWidget::TimeMilageWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimeMilageWidget),
    mUser(nullptr),
    mTimeMilageModel(nullptr)
{
    ui->setupUi(this);
    ui->pTimeMilageView->setFocus();

    connect(ui->pTimeMilageView, &QTreeView::clicked, this, &TimeMilageWidget::clickedOn);
    connect(this, &TimeMilageWidget::activitySelected, &mTimer,&TMTTimer::activitySelected);
    connect(&mTimer, &TMTTimer::currentDayChanged, this, &TimeMilageWidget::setCurrentDay);
    connect(ui->pPauseBtn, &QPushButton::clicked, this, &TimeMilageWidget::pauseClicked);
    connect(ui->pPauseBtn, &QPushButton::clicked, &mTimer, &TMTTimer::setPaused);
    connect(&mTimer, &TMTTimer::resumed, this, &TimeMilageWidget::resumedActivity);


}

TimeMilageWidget::~TimeMilageWidget()
{
    delete ui;
}

void TimeMilageWidget::setProfile(User* user)
{

    mUser = user;

    connect(&mTimer, &TMTTimer::timeToSave, mUser, &User::doSave);
    connect(this, &TimeMilageWidget::save, mUser,  &User::doSave);

    mTimeMilageModel = new TimeMilageModel(mUser, mTimer.currentDay());

    connect(this, &TimeMilageWidget::currentDayChanged, mTimeMilageModel, &TimeMilageModel::setCurrentDay);

    ui->pTimeMilageView->setModel(mTimeMilageModel);
    ui->pTimeMilageView->expandAll();

    QHeaderView* header = ui->pTimeMilageView->header();
    if (header)
    {
        int logicalIndex = header->logicalIndex(0);
        header->setSectionResizeMode(logicalIndex, QHeaderView::Stretch);
    }

	// Automatically start timer on first row.
//    TMTActivity* firstActivity = mTimeMilageModel->firstActivity();
//    if (firstActivity)
//    {
//        QModelIndex firstRow = mTimeMilageModel->indexFromItem(firstActivity);
//        ui->pTimeMilageView->setCurrentIndex(firstRow);
//        clickedOn(firstRow);
//    }


}

bool TimeMilageWidget::isActive() const
{
    return mTimer.isActive();

}

void TimeMilageWidget::setActive(bool active)
{

    mTimer.setActive(active);

}

void TimeMilageWidget::clickedOn(const QModelIndex &index)
{


    QModelIndex     activityIndex = index.siblingAtColumn(0);

    TMTActivity* activity = dynamic_cast<TMTActivity*>(mTimeMilageModel->itemFromIndex(activityIndex));

    if (activity)
    {
        emit activitySelected(activity);

        // In case we are in puased-mode, reset the pause-button.
        QFont font = ui->pPauseBtn->font();
        font.setBold(false);
        ui->pPauseBtn->setFont(font);
        ui->pPauseBtn->setChecked(false);

    }

}

void TimeMilageWidget::setCurrentDay(JulianDay day)
{

    emit currentDayChanged(day);

}


void TimeMilageWidget::pauseClicked(bool checked)
{
    QFont font = ui->pPauseBtn->font();

    if (checked == true)
    {
        font.setBold(true);
        ui->pPauseBtn->setFont(font);
        ui->pTimeMilageView->selectionModel()->clear();
    }
    else
    {
        font.setBold(false);
        ui->pPauseBtn->setFont(font);
    }
}

void TimeMilageWidget::resumedActivity(TMTActivity *activity)
{
    QFont font = ui->pPauseBtn->font();

    font.setBold(false);
    ui->pPauseBtn->setFont(font);
    ui->pPauseBtn->setChecked(false);

    QModelIndex selection = mTimeMilageModel->indexFromItem(activity);
    ui->pTimeMilageView->setCurrentIndex(selection);

    ui->pTimeMilageView->setFocus();

}

