/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "tmtcontext.h"
#include "tmttimesummary.h"

#include "repository/context.h"
#include "repository/activity.h"

#include "tmtactivity.h"


#include <QtDebug>

TMTContext::TMTContext(TMTTimeSummary* totalSummary, Context* source):
    QStandardItem(safeContextName(source)),
    mSource(source),
    mTimeSummary(new TMTTimeSummary),
    mTotalSummary(totalSummary)
{

    if (!mSource)
        return;

    connect(mSource, &Context::activityAdded, this, &TMTContext::addActivity);

    setColumnCount(3);

    setSelectable(false);       // Contexts is not selctable - does not count time.

    foreach(QString activityName, mSource->activityNames())
    {
        Activity* activity = source->activity(activityName, false);
        addActivity(activity);

    }
}

QList<QStandardItem *> TMTContext::row()
{
    QList<QStandardItem*> result;
    result.append(this);

    if (mTimeSummary)
        result.append(mTimeSummary);
    else
        result.append(new QStandardItem);

    // Will be the milage summary later on.
    result.append(new QStandardItem);

    return result;
}

void TMTContext::setCurrentDay(JulianDay day)
{
    emit currentDayChanged(day);
}

void TMTContext::addActivity(Activity *activity)
{
    if (activity)
    {
        TMTActivity* child = new TMTActivity(activity);
        connect(this, &TMTContext::currentDayChanged, child, &TMTActivity::setCurrentDay);
        connect(child, &TMTActivity::itemChanged, this, &TMTContext::itemChanged);

        appendRow(child->row());

        mTimeSummary->track(child->timeEntry());
        if (mTotalSummary)
            mTotalSummary->track(child->timeEntry());

    }

}

QString TMTContext::safeContextName(Context *source) const
{
    QString result;

    if (source)
        result = source->name();

    return result;

}
