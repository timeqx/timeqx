/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TIMEMILAGETRACKER_H
#define TIMEMILAGETRACKER_H

#include "timemilagemodel.h"
#include "tmttimer.h"

#include <QPointer>
#include <QWidget>

class User;
class TMTActivity;

namespace Ui {
class TimeMilageWidget;
}

class TimeMilageWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit TimeMilageWidget(QWidget *parent = nullptr);
        ~TimeMilageWidget();

        void setProfile(User* user);
        bool isActive() const;

    signals:

        void currentDayChanged(JulianDay day);
        void activitySelected(TMTActivity* activity);
        void save();

    public slots:
        void setActive(bool active = true);
        void clickedOn(const QModelIndex& index);
        void setCurrentDay(JulianDay day);

    protected slots:
        void pauseClicked(bool checked);
        void resumedActivity(TMTActivity* activity);

    private:
        Ui::TimeMilageWidget    *ui;
        QPointer<User>          mUser;
        TimeMilageModel         *mTimeMilageModel;
        TMTTimer                mTimer;
};

#endif // TIMEMILAGETRACKER_H
