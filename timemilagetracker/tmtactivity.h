/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TMTACTIVITY_H
#define TMTACTIVITY_H

#include <QObject>
#include <QStandardItem>

#include "repository/julianday.h"

#include <tmttimeentry.h>

class Activity;

class TMTActivity : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit TMTActivity(Activity * source);

//        virtual int columnCount() const;
//        virtual int rowCount() const;
//        virtual bool hasChildren() const;
//        virtual QStandardItem *	child(int row, int column = 0) const;

        QList<QStandardItem*> row();

        QString activityName() const;
        QString contextName() const;

        Activity* activity();

        TMTTimeEntry* timeEntry();

    signals:
        void currentDayChanged(JulianDay day);
        void itemChanged(QStandardItem *item);


    public slots:

        void setCurrentDay(JulianDay day);

    private:
        Activity*       mSource;
        QPointer<TMTTimeEntry>   mTimeEntry;
        QPointer<TMTTimeEntry>   mMilageEntry;
};

#endif // TMTACTIVITY_H
