/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "tmtactivity.h"

#include <repository/activity.h>
#include <repository/context.h>

TMTActivity::TMTActivity(Activity *source):
    QObject(nullptr),
    mSource(source)
{
    setColumnCount(3);

    if (!mSource)
        return;

    setText(mSource->name());

    mTimeEntry = new TMTTimeEntry(mSource);
    connect(this, &TMTActivity::currentDayChanged, mTimeEntry, &TMTTimeEntry::setCurrentDay);
    connect(mTimeEntry, &TMTTimeEntry::itemChanged, this, &TMTActivity::itemChanged);

}


QList<QStandardItem *> TMTActivity::row()
{
    QList<QStandardItem*> result;

    result.append(this);

    if (mTimeEntry)
        result.append(mTimeEntry);
    else
        result.append(new QStandardItem);

    if (mMilageEntry)
        result.append(mMilageEntry);
    else
        result.append(new QStandardItem);

    return result;


}

QString TMTActivity::activityName() const
{
    return mSource->name();
}

QString TMTActivity::contextName() const
{
    return mSource->parent()->name();
}

Activity *TMTActivity::activity()
{

    return mSource;

}

TMTTimeEntry *TMTActivity::timeEntry()
{
    return mTimeEntry.data();

}

void TMTActivity::setCurrentDay(JulianDay day)
{
    emit currentDayChanged(day);

}
