/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TMTCONTEXT_H
#define TMTCONTEXT_H

#include <QObject>
#include <QPointer>
#include <QStandardItem>

#include "repository/julianday.h"


class Context;
class Activity;
class TMTTimeSummary;

class TMTContext : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit TMTContext(TMTTimeSummary* totalSummary, Context* source);

        QList<QStandardItem*> row();


    signals:
        void currentDayChanged(JulianDay day);
        void itemChanged(QStandardItem *item);
        void timeUpdated();

    public slots:

        void setCurrentDay(JulianDay day);
        void addActivity(Activity* activity);

    private:
        QString safeContextName(Context* source) const;

    private:
        QPointer<Context>               mSource;
        QPointer<TMTTimeSummary>        mTimeSummary;
        QPointer<TMTTimeSummary>        mTotalSummary;

};

#endif // TMTCONTEXT_H
