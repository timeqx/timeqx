#-----------------------------------------------------------------------
# This file is part of Timeqx.
#
# Copyright 2018-2019 Erik Ridderby
#
# Neville the Remeberall is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Timeqx is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with Neville the Remeberall.  If not, see
# <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------

QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = timeqx
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH +=  timemilagetracker/ \
                repository/

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    timemilagetracker/timemilagewidget.cpp \
    repository/user.cpp \
    repository/context.cpp \
    repository/activity.cpp \
    repository/timemilagerecord.cpp \
    timemilagetracker/timemilagemodel.cpp \
    timemilagetracker/tmtcontext.cpp \
    timemilagetracker/tmtactivity.cpp \
    timemilagetracker/tmttimeentry.cpp \
    repository/trackingrecord.cpp \
    timemilagetracker/tmttimer.cpp \
    timemilagetracker/treeview.cpp \
    timemilagetracker/tmttimesummary.cpp \
    timemilagetracker/tmttotalsummary.cpp \
    weektimesummarydialog/weektimesummarydialog.cpp \
    weektimesummarydialog/wtscontext.cpp \
    weektimesummarydialog/wtsactivity.cpp \
    weektimesummarydialog/weektimesummarymodel.cpp \
    weektimesummarydialog/wtssummaryrow.cpp

HEADERS += \
        mainwindow.h \
    timemilagetracker/timemilagewidget.h \
    repository/user.h \
    repository/context.h \
    repository/activity.h \
    repository/timemilagerecord.h \
    repository/julianday.h \
    timemilagetracker/timemilagemodel.h \
    timemilagetracker/tmtcontext.h \
    timemilagetracker/tmtactivity.h \
    timemilagetracker/tmttimeentry.h \
    repository/trackingrecord.h \
    timemilagetracker/tmttimer.h \
    timemilagetracker/treeview.h \
    timemilagetracker/tmttimesummary.h \
    timemilagetracker/tmttotalsummary.h \
    weektimesummarydialog/weektimesummarydialog.h \
    weektimesummarydialog/wtscontext.h \
    weektimesummarydialog/wtsactivity.h \
    weektimesummarydialog/weektimesummarymodel.h \
    weektimesummarydialog/wtssummaryrow.h

FORMS += \
        mainwindow.ui \
    timemilagetracker/timemilagewidget.ui \
    weektimesummarydialog/weektimesummarydialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
