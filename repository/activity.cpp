/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "activity.h"

#include "timemilagerecord.h"
#include "user.h"
#include "context.h"

#include <QDate>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <QtDebug>

Activity::Activity(Context *parent, const QString &name, bool charge):
    mParent(parent),
    mName(name),
    mCharge(charge)
{

}

Activity::Activity(Context *parent, QJsonObject object):
    mParent(parent),
    mCharge(true)
{
    QString name = object.value("Activity").toString();
    QString charge = object.value("Charge").toString();

    if (!name.isNull() && !name.isEmpty())
    {
        mName = name;

        if (charge.compare("false", Qt::CaseInsensitive) == 0)
        {
            mCharge = false;
        }
        else
        {
            mCharge = true;
        }
    }

}

QString Activity::name() const
{
    return mName;
}

void Activity::setName(const QString &name)
{
    mName = name;
}

bool Activity::sameAs(const QString &otherContext) const
{
    return mName == otherContext;
}

bool Activity::sameAs(const Activity &otherContext) const
{

    return mName == otherContext.mName;
}

bool Activity::addRecord(TimeMilageRecord *record)
{
    if (!record)
        return false;

    if (mRecords.contains(record->day()))
            return false;

    mRecords.insert(record->day(), record);

    return true;
}

TimeMilageRecord *Activity::record(JulianDay day, bool createIfNonExisting)
{
    if (mRecords.contains(day))
        return mRecords.value(day);

    if (createIfNonExisting)
    {
        TimeMilageRecord* record = new TimeMilageRecord(day);
        mRecords.insert(day, record);

        return record;
    }
    else
        return nullptr;
}

bool Activity::charge() const
{
    return mCharge;
}

void Activity::setCharge(bool charge)
{
    mCharge = charge;
}

Context* Activity::parent() const
{
    return mParent;
}

