/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "user.h"

#include <QApplication>
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "context.h"
#include "activity.h"
#include "trackingrecord.h"

#include <QtDebug>
User::User(const QString &tag) :
    QObject(nullptr),
    mUserTag(tag)
{

}

void User::addContext(Context *context, bool replaceIfExist)
{
    if (context && (replaceIfExist || !mContexts.contains(context->name())))
        mContexts.insert(context->name(), context);

}

Context *User::context(const QString &contextName, bool createIfNonExisting)
{
    if (mContexts.contains(contextName))
        return mContexts.value(contextName);

    if (createIfNonExisting)
    {
        mContexts.insert(contextName, new Context(this, contextName));
        emit contextAdded(mContexts.value(contextName));
        return mContexts.value(contextName);
    }

    return nullptr;

}

QStringList User::contextNames() const
{
    return mContexts.keys();

}

QList<Context *> User::contexts()
{
    return mContexts.values();

}

void User::readFromDisk(const QString &basePath)
{
    QDir baseDir(basePath);
    baseDir.cd("users/" + mUserTag);

    mUserPath = baseDir.path();

    QFile userFile(baseDir.filePath("user.json"));

    userFile.open(QIODevice::ReadOnly | QIODevice::Text | QIODevice::Unbuffered);

    QByteArray userBa = userFile.readAll();

    QJsonDocument user = QJsonDocument::fromJson(userBa);

    QJsonObject userFrame = user.object();
    QJsonObject userObject = userFrame.value("User").toObject();


    QString     userName = userObject.value("UserName").toString();
    QString     userTag  = userObject.value("UserTag").toString();


    QJsonArray  projectArray = userObject.value("Contexts").toArray();


    for(QJsonArray::const_iterator i = projectArray.constBegin(); i != projectArray.constEnd() ;i++)
    {
        QJsonObject contextObject = (*i).toObject();
        Context* context= new Context(this, contextObject);

        if (context)
        {
            mContexts.insert(context->name(), context);
        }
    }

}

QString User::userPath() const
{
    return mUserPath;
}

QString User::trackingPath() const
{
    return QString("%1/Tracker").arg(userPath());
}

TrackingRecord *User::trackingRecord(JulianDay day)
{
    if (!mTrackingRecords.contains(day))
    {
        TrackingRecord* record = new TrackingRecord(this, day);
        connect(this, &User::save, record, &TrackingRecord::doSave);
        mTrackingRecords.insert(day, record);
    }

    return mTrackingRecords.value(day);
}

void User::doSave()
{    
    emit save();
}
