/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef USER_H
#define USER_H

#include <QObject>
#include <QHash>
#include <QMap>

#include "julianday.h"

class Context;
class TrackingRecord;

class User : public QObject
{
        Q_OBJECT
    public:
        explicit User(const QString& tag);

        void addContext(Context* context, bool replaceIfExist = false);
        Context* context(const QString& contextName, bool createIfNonExisting = true);

        QStringList         contextNames() const;
        QList<Context*>     contexts();

        void readFromDisk(const QString& basePath);

        QString userPath() const;
        QString trackingPath() const;

        TrackingRecord* trackingRecord(JulianDay day);

    signals:

        void contextAdded(Context* context);
        void save();

    public slots:

        void doSave();

    private:

        QString                             mUserName;
        QString                             mUserTag;
        QString                             mUserPath;
        QMap<QString, Context*>             mContexts;
        QHash<JulianDay, TrackingRecord*>   mTrackingRecords;

};

#endif // USER_H
