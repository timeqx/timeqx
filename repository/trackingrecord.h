/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TRACKINGRECORD_H
#define TRACKINGRECORD_H

#include <QObject>
#include <QPointer>
#include <QDir>
#include "julianday.h"

class User;
class TimeMilageRecord;

class TrackingRecord : public QObject
{
        Q_OBJECT
    public:
        explicit TrackingRecord(User* user, JulianDay day);

    signals:

    public slots:

        void doRead();
        void doSave();

    protected:

        QString formatTime(const QTime& time) const;
        QTime   parseTime(const QString& input) const;

        QDir filePath();
        QString fileName();

        QDate   date();

    private:

        QPointer<User>              mUser;
        JulianDay                   mDay;
        QList<TimeMilageRecord*>    mRecords;

};

#endif // TRACKINGRECORD_H
