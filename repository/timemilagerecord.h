/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef TIMEMILAGERECORD_H
#define TIMEMILAGERECORD_H

#include <QObject>
#include <QTime>
#include "julianday.h"

class TimeMilageRecord : public QObject
{
        Q_OBJECT
    public:
        explicit TimeMilageRecord(JulianDay day, const QTime& time = QTime(0,0,0,0), int milage = 0);

        JulianDay day() const;
        float milage() const;
        QTime time() const;

        void setDirty();
        void setClean();
        bool isDirty() const;


    signals:

        void timeChanged(const QTime& newTime);
        void milageChanged(const float milage);

    public slots:

        void setMilage(float milage);
        void setTime(const QTime& time);
        int addTime(int seconds);

    private:
        JulianDay   mDay;
        QTime       mTime;
        float       mMilage;

        bool        mIsDirty;
};

#endif // TIMEMILAGERECORD_H
