/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "context.h"

#include "activity.h"
#include "user.h"

#include <QJsonObject>
#include <QJsonArray>


#include <QtDebug>

Context::Context(User* user, const QString &name) :
    QObject(nullptr),
    mUser(user),
    mName(name)
{

}

Context::Context(User* user, QJsonObject object):
    mUser(user)
{
    mName = object.value("Context").toString();

    QJsonArray  activityArray = object.value("Activities").toArray();


    for(QJsonArray::const_iterator i = activityArray.constBegin(); i != activityArray.constEnd() ;i++)
    {
        QJsonObject activityObject = (*i).toObject();
        Activity* activity = new Activity(this, activityObject);

        if (activity)
        {
            mActivities.insert(activity->name(), activity);
        }
    }


}

bool Context::sameAs(const QString &otherContext) const
{
    return mName == otherContext;
}

bool Context::sameAs(const Context &otherContext) const
{
    return mName == otherContext.mName;
}

QString Context::name() const
{
    return mName;
}

void Context::setName(const QString &name)
{
    mName = name;
}

void Context::addActivity(Activity *activity, bool replaceIfExist)
{
    if (activity && (replaceIfExist || !mActivities.contains(activity->name())))
        mActivities.insert(activity->name(), activity);

}

Activity *Context::activity(const QString &activityName, bool createIfNonExisting)
{

    if (mActivities.contains(activityName))
        return mActivities.value(activityName);

    if (createIfNonExisting)
    {
        mActivities.insert(activityName, new Activity(this, activityName));

        emit activityAdded(mActivities.value(activityName));
        return mActivities.value(activityName);
    }

    return nullptr;

}

QStringList Context::activityNames() const
{
    return mActivities.keys();
}

QList<Activity *> Context::activities()
{
    return mActivities.values();
}
