/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "trackingrecord.h"

#include <QDate>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "user.h"
#include "context.h"
#include "activity.h"
#include "timemilagerecord.h"


#include <QtDebug>

TrackingRecord::TrackingRecord(User *user, JulianDay day):
    mUser(user),
    mDay(day)
{

    if (!mUser)
        return;

    connect(user, &User::save, this, &TrackingRecord::doSave);

    doRead();

}

void TrackingRecord::doRead()
{

    QFile   file(fileName());

    file.open(QIODevice::ReadOnly | QIODevice::Text | QIODevice::Unbuffered);

    if (file.isOpen() || file.isReadable())
    {
        QByteArray fileContent = file.readAll();
        file.close();

        QJsonArray readRecords = QJsonDocument::fromJson(fileContent).array();

        for(QJsonArray::const_iterator i = readRecords.constBegin(); i != readRecords.constEnd() ;i++)
        {
            QJsonObject recordObject = (*i).toObject();

            QString readContext = recordObject.value("Context").toString();
            QString readActivity = recordObject.value("Activity").toString();
            QTime   readTimeSpent = parseTime(recordObject.value("TimeSpent").toString());


            if (!readContext.isEmpty() && !readActivity.isEmpty())              // Wer have a decent record
            {
                Context* pContext = mUser->context(readContext,true);           // Find or create, we don't care.
                if (!pContext)
                    return;

                Activity* pActivity = pContext->activity(readActivity, true);   // Find or create, we don't care.
                if (!pActivity)
                    return;


                TimeMilageRecord* pTimeMilageRecord = pActivity->record(mDay, true); // Find or create, we don't care.

                if (!pTimeMilageRecord)
                    return;

                pTimeMilageRecord->setTime(readTimeSpent);


                mRecords.append(pTimeMilageRecord);

            }
        }

    }
}



void TrackingRecord::doSave()
{
    QList<TimeMilageRecord*> recordsIncluded;
    bool bAnyDirty = false;

    QJsonArray  records;

    foreach(Context* pContext, mUser->contexts())
    {
        if (pContext)
        {
            foreach(Activity* pActivity, pContext->activities())
            {
                if (pActivity)
                {
                    TimeMilageRecord* tmRecord = pActivity->record(mDay, false);
                    if (tmRecord && tmRecord->time() > QTime(0,0,10,0))
                    {
                        bAnyDirty |= tmRecord->isDirty();

                        QJsonObject record;

                        record["Context"]   = pContext->name();
                        record["Activity"]  = pActivity->name();
                        record["TimeSpent"] = formatTime(tmRecord->time());

                        records.append(record);

                        recordsIncluded.append(tmRecord);
                    }
                }
            }
        }
    }

    // We only save if any of the included items are dirty
    if (records.count() > 0 && bAnyDirty)
    {

        QJsonDocument frame(records);

        QStringList print = QString::fromUtf8(frame.toJson()).split('\n');

        QDir path = filePath();
        if (!path.exists())
        {
            QString newPath = path.path();
            path.mkpath(newPath);
        }

        QFile   file(fileName());

        file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Unbuffered);

        if (file.isOpen() || file.isWritable())
        {
            QByteArray content = frame.toJson();

            if (file.write(content) >= content.size())
            {
                // File is written OK, clean the dirtyflag.
                foreach(TimeMilageRecord* tmRecord, recordsIncluded)
                    if (tmRecord)
                        tmRecord->setClean();
            }
        }

        file.close();

    }


}

QString TrackingRecord::formatTime(const QTime &time) const
{
    return time.toString("H:mm");
}


QTime TrackingRecord::parseTime(const QString &input) const
{
    QTime result = QTime::fromString(input, "H:mm");

    if (!result.isValid())
        result = QTime::fromString(input, "HH:mm");

    if (!result.isValid())
        result = QTime::fromString(input, "H:mm:ss");

    if (!result.isValid())
        result = QTime::fromString(input, "HH:mm:ss");

    return result;
}


QDir TrackingRecord::filePath()
{

    QString expectedFilePath = QString("%1/%2").arg(mUser->trackingPath()).arg(date().toString("yyyy/MM"));
    QDir    path(expectedFilePath);

    return path;
}

QString TrackingRecord::fileName()
{

    QString expectedFileName = QString("%1.json").arg(date().toString("yyyyMMdd"));
    return filePath().filePath(expectedFileName);
}

QDate TrackingRecord::date()
{
    return QDate::fromJulianDay(mDay);
}


