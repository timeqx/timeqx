/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef CONTEXT_H
#define CONTEXT_H

#include <QObject>
#include <QMap>
#include <QJsonObject>
#include <QPointer>

class User;
class Activity;

class Context : public QObject
{
        Q_OBJECT
    public:
        explicit Context(User* user, const QString& name);
        explicit Context(User* user, QJsonObject object);

        bool sameAs(const QString& otherContext) const;
        bool sameAs(const Context& otherContext) const;

        QString name() const;
        void setName(const QString &name);

        void addActivity(Activity* activity, bool replaceIfExist = false);
        Activity* activity(const QString& activityName, bool createIfNonExisting = true);

        QStringList activityNames() const;
        QList<Activity*> activities();


    signals:

        void activityAdded(Activity* activity);

    public slots:

    private:
        QPointer<User>              mUser;
        QString                     mName;
        QMap<QString, Activity*>   mActivities;
};

#endif // CONTEXT_H
