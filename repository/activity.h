/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <QHash>
#include <QJsonObject>
#include <QObject>
#include <QPointer>

#include "julianday.h"

class Context;
class TimeMilageRecord;

class Activity : public QObject
{
        Q_OBJECT

    public:
        typedef QHash<JulianDay, TimeMilageRecord*> DayRecordMap;

    public:
        explicit Activity(Context* parent, const QString& name, bool charge = true);
        explicit Activity(Context* parent, QJsonObject object);

        QString name() const;
        void setName(const QString &name);

        bool sameAs(const QString& otherContext) const;
        bool sameAs(const Activity& otherContext) const;

        bool addRecord(TimeMilageRecord* record);

        TimeMilageRecord* record(JulianDay day, bool createIfNonExisting = true);


        bool charge() const;
        void setCharge(bool charge);

        Context* parent() const;

    signals:


    public slots:

    private:

        QPointer<Context>       mParent;
        QString                 mName;
        bool                    mCharge;
        DayRecordMap            mRecords;

};

#endif // ACTIVITY_H
