/***********************************************************************
This file is part of Timeqx.

Copyright 2018-2019 Erik Ridderby

Neville the Remeberall is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Timeqx is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Neville the Remeberall.  If not, see
<http://www.gnu.org/licenses/>.

***********************************************************************/

#include "timemilagerecord.h"

#include <QTime>



TimeMilageRecord::TimeMilageRecord(JulianDay day, const QTime &time, int milage):
    mDay(day),
    mTime(time),
    mMilage(milage),
    mIsDirty(false)
{

}

float TimeMilageRecord::milage() const
{
    return mMilage;
}

void TimeMilageRecord::setDirty()
{
    mIsDirty = true;
}

void TimeMilageRecord::setClean()
{
    mIsDirty = false;

}

bool TimeMilageRecord::isDirty() const
{
    return mIsDirty;

}

QTime TimeMilageRecord::time() const
{
    return mTime;
}

void TimeMilageRecord::setMilage(float milage)
{
    setDirty();
    mMilage = milage;

    emit milageChanged(mMilage);
}

void TimeMilageRecord::setTime(const QTime &time)
{
    setDirty();

    mTime = time;
    emit timeChanged(mTime);

}

int TimeMilageRecord::addTime(int seconds)
{
    int currentSeconds = QTime(0,0,0,0).secsTo(mTime);

    if (seconds < 0 && seconds < -1 * currentSeconds)
            seconds = -1 * currentSeconds;

    setTime(mTime.addSecs(seconds));

    return seconds;
}

JulianDay TimeMilageRecord::day() const
{
    return mDay;
}
