#ifndef WEEKTIMESUMMARYMODEL_H
#define WEEKTIMESUMMARYMODEL_H

#include <QObject>
#include <QStandardItemModel>
#include <QPointer>

#include "repository/julianday.h"

class User;
class Context;
class WTSSummaryRow;

class WeekTimeSummaryModel : public QStandardItemModel
{
        Q_OBJECT
    public:
        explicit WeekTimeSummaryModel(User* user, JulianDay day);

    signals:
        void currentDayChanged(JulianDay day);

    public slots:
        void setCurrentDay(JulianDay day);
        void addContext(Context* context);

    private:
        QPointer<User>          mProfile;
        JulianDay               mCurrentDay;
        WTSSummaryRow           *mSummaryRow;



};

#endif // WEEKTIMESUMMARYMODEL_H
