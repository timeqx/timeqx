#include "wtsactivity.h"
#include "wtssummaryrow.h"

#include "repository/activity.h"

#include "timemilagetracker/tmttimeentry.h"
#include "timemilagetracker/tmttimesummary.h"

WTSActivity::WTSActivity(WTSSummaryRow* summaryRow, Activity *source):
    mSummaryRow(summaryRow),
    mWeekSummary(new TMTTimeSummary)
{
    setColumnCount(9);

    setText(source->name());

    for (int i = 0; i < 7; i++)
    {
        TMTTimeEntry *entry = new TMTTimeEntry(source);
        mWeekSummary->track(entry);
        summaryRow->trackDay(i, entry);
        mEntries.append(entry);
    }

}

QList<QStandardItem *> WTSActivity::row()
{
    QList<QStandardItem*> result;
    result.append(this);
    result.append(mWeekSummary);

    for (int i = 0; i < 7; i++)
    {
        result.append(mEntries.at(i));
    }

    return result;
}

QList<QPointer<TMTTimeEntry> > WTSActivity::entries()
{
    return mEntries;
}

void WTSActivity::setCurrentDay(JulianDay day)
{

    for (int i = 0; i < 7; i++)
    {
        mEntries.at(i)->setCurrentDay(day + i);
    }

}


