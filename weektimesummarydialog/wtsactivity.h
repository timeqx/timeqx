#ifndef WTSACTIVITY_H
#define WTSACTIVITY_H

#include <QObject>
#include <QPointer>
#include <QStandardItem>

#include "repository/julianday.h"

class Activity;
class TMTTimeEntry;
class TMTTimeSummary;
class WTSSummaryRow;

class WTSActivity : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit WTSActivity(WTSSummaryRow* summaryRow, Activity * source);
        QList<QStandardItem *> row();

        QList< QPointer<TMTTimeEntry> > entries();


    signals:

    public slots:
        void setCurrentDay(JulianDay day);

    private:

        QPointer<WTSSummaryRow>             mSummaryRow;
        QList< QPointer<TMTTimeEntry> >     mEntries;
        TMTTimeSummary                      *mWeekSummary;
};

#endif // WTSACTIVITY_H
