#include "weektimesummarymodel.h"
#include "wtscontext.h"

#include "repository/user.h"
#include "repository/context.h"

#include "wtssummaryrow.h"

WeekTimeSummaryModel::WeekTimeSummaryModel(User *user, JulianDay day):
    mProfile(user),
    mCurrentDay(day),
    mSummaryRow(new WTSSummaryRow)
{
//    connect(mUser, &User::contextAdded, this, &WeekTimeSummaryModel::addContext);

    setColumnCount(9);

    QStringList hHeader;
    hHeader << "Aktivitet" << "Total" << "Må" << "Ti" << "On" << "To" << "Fr" << "Lö" << "Sö";
    setHorizontalHeaderLabels(hHeader);

    // Add the total summary as the first row.

//    appendRow(mTotalSummary->row());

    appendRow(mSummaryRow->row());

    if (mProfile)
    {
        foreach(QString contextName, mProfile->contextNames())
        {
            Context *context = mProfile->context(contextName, false);
            addContext(context);

        }
    }

    setCurrentDay(mCurrentDay);
}

void WeekTimeSummaryModel::setCurrentDay(JulianDay day)
{
    if (!mProfile)
        return;

    connect(mProfile, &User::contextAdded, this, &WeekTimeSummaryModel::addContext);

    // Ensure that existing tracking reckords are loaded
    for (int i = 0; i < 7; i++)
        mProfile->trackingRecord(day + i);

    disconnect(mProfile, &User::contextAdded, this, &WeekTimeSummaryModel::addContext);

    emit currentDayChanged(day);

}

void WeekTimeSummaryModel::addContext(Context *context)
{
    if (context)
    {
        WTSContext* tmtContext = new WTSContext(mSummaryRow, context);
        appendRow(tmtContext->row());

        connect(this, &WeekTimeSummaryModel::currentDayChanged, tmtContext, &WTSContext::setCurrentDay);
    }

}

