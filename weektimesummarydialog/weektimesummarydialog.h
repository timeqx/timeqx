#ifndef TIMESUMMARYDIALOG_H
#define TIMESUMMARYDIALOG_H

#include <QDialog>
#include <QPointer>

#include "repository/julianday.h"

namespace Ui {
class WeekTimeSummaryDialog;
}

class User;
class WeekTimeSummaryModel;

class WeekTimeSummaryDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit WeekTimeSummaryDialog(QWidget *parent = nullptr);
        ~WeekTimeSummaryDialog();

        void setProfile(User* user);

    public slots:
        void weekForward();
        void weekBackward();

    protected slots:
        void dateSelected(const QDate& date);

    protected:
        JulianDay   firstDayOfSelectedWeek() const;

    private:
        Ui::WeekTimeSummaryDialog   *ui;
        QPointer<User>              mProfile;
        WeekTimeSummaryModel        *mModel;
};

#endif // TIMESUMMARYDIALOG_H
