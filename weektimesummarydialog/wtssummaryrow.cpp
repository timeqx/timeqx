#include "wtssummaryrow.h"

#include <tmttimeentry.h>
#include <tmttimesummary.h>

WTSSummaryRow::WTSSummaryRow():
    QStandardItem ("Totalt:"),
    mWeekSummary(new TMTTimeSummary)
{

    for(int i = 0; i < 7; i++)
    {
        TMTTimeSummary* summary = new TMTTimeSummary;
        mWeekSummary->follow(summary);
        mDailySummaries.append(summary);
    }

}

QList<QStandardItem *> WTSSummaryRow::row()
{
    QList<QStandardItem *>  result;

    result.append(this);
    result.append(mWeekSummary);
    foreach(TMTTimeSummary* item, mDailySummaries)
        result.append(item);

    return result;

}

void WTSSummaryRow::trackDay(int dayInWeek, TMTTimeEntry *entry)
{
    // We steer this programatically, out of bound will clearly crash and imply a bug
    mDailySummaries[dayInWeek]->track(entry);
    emit trackedAtDay(dayInWeek, entry);
    emit tracked(entry);
}
