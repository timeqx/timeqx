#include "weektimesummarydialog.h"
#include "ui_weektimesummarydialog.h"

#include "repository/user.h"
#include "weektimesummarymodel.h"

#include <QCalendarWidget>

#include <QtDebug>

WeekTimeSummaryDialog::WeekTimeSummaryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WeekTimeSummaryDialog),
    mModel(nullptr)
{
    ui->setupUi(this);
    ui->pDateSelector->setDate(QDate::currentDate());
    dateSelected(QDate::currentDate());

    connect(ui->pDateSelector, &QDateTimeEdit::dateChanged, this, &WeekTimeSummaryDialog::dateSelected);
    connect(ui->pForwardButton, &QPushButton::clicked, this, &WeekTimeSummaryDialog::weekForward);
    connect(ui->pBackwardButton, &QPushButton::clicked, this, &WeekTimeSummaryDialog::weekBackward);

    QCalendarWidget* calendarWidget = ui->pDateSelector->calendarWidget();
    if (calendarWidget)
    {
        calendarWidget->setVerticalHeaderFormat(QCalendarWidget::ISOWeekNumbers);
    }


}

WeekTimeSummaryDialog::~WeekTimeSummaryDialog()
{
    delete ui;
}

void WeekTimeSummaryDialog::setProfile(User *user)
{
    mProfile = user;

    if (mModel)
        return;

    // TODO: Make proper cleaning in case there already is a model set.
    mModel = new WeekTimeSummaryModel(mProfile, firstDayOfSelectedWeek());

    ui->pSummaryView->setModel(mModel);
    ui->pSummaryView->expandAll();


    QHeaderView* header = ui->pSummaryView->header();
    if (header)
    {
        int logicalIndex = header->logicalIndex(0);
        header->setSectionResizeMode(logicalIndex, QHeaderView::Stretch);
    }

    mModel->setCurrentDay(firstDayOfSelectedWeek());


}

void WeekTimeSummaryDialog::weekForward()
{
    ui->pDateSelector->setDate(ui->pDateSelector->date().addDays(7));
//    dateSelected(ui->pDateSelector->date());

}

void WeekTimeSummaryDialog::weekBackward()
{
    ui->pDateSelector->setDate(ui->pDateSelector->date().addDays(-7));
//    dateSelected(ui->pDateSelector->date());
}

void WeekTimeSummaryDialog::dateSelected(const QDate &date)
{
    Q_UNUSED(date)

    if (mModel)
    {
        mModel->setCurrentDay(firstDayOfSelectedWeek());
        ui->pSummaryView->expandAll();
    }
}


JulianDay WeekTimeSummaryDialog::firstDayOfSelectedWeek() const
{
    QDate date = ui->pDateSelector->date();
    return date.addDays(1 - date.dayOfWeek()).toJulianDay();

}
