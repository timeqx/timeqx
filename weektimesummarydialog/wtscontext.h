#ifndef WTSCONTEXT_H
#define WTSCONTEXT_H

#include <QObject>
#include <QPointer>
#include <QStandardItem>
#include <QList>

#include "repository/julianday.h"

class TMTTimeSummary;
class Context;
class Activity;
class WTSSummaryRow;
class TMTTimeEntry;

class WTSContext : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit WTSContext(WTSSummaryRow* summaryRow, Context* source);
        QList<QStandardItem*> row();

    signals:
        void currentDayChanged(JulianDay day);

    public slots:
        void setCurrentDay(JulianDay day);
        void addActivity(Activity* activity);

        void trackDay(int dayInWeek, TMTTimeEntry* entry);

    private:
        QPointer<Context>                   mSource;
        QPointer<WTSSummaryRow>             mSummaryRow;

        QList<TMTTimeSummary*>              mDailyContextSummary;
        TMTTimeSummary                      *mContextWeekTotal;
};

#endif // WTSCONTEXT_H
