#ifndef WTSSUMMARYROW_H
#define WTSSUMMARYROW_H

#include <QObject>
#include <QStandardItem>

class TMTTimeSummary;
class TMTTimeEntry;

class WTSSummaryRow : public QObject, public QStandardItem
{
        Q_OBJECT
    public:
        explicit WTSSummaryRow();
        QList<QStandardItem*> row();



    signals:
        void trackedAtDay(int dayInWeek, TMTTimeEntry* entry);
        void tracked(TMTTimeEntry* entry);

    public slots:
        void trackDay(int dayInWeek, TMTTimeEntry* entry);
    private:

        QList<TMTTimeSummary*>  mDailySummaries;
        TMTTimeSummary*         mWeekSummary;

};

#endif // WTSSUMMARYROW_H
