#include "wtscontext.h"

#include "timemilagetracker/tmttimesummary.h"
#include "repository/context.h"
#include "repository/activity.h"

#include "wtsactivity.h"
#include "wtscontext.h"
#include "wtssummaryrow.h"
#include "timemilagetracker/tmttimeentry.h"

WTSContext::WTSContext(WTSSummaryRow *summaryRow, Context* source) :
    QObject(nullptr),
    QStandardItem (source->name()),
    mSource(source),
    mSummaryRow(summaryRow),
    mContextWeekTotal(new TMTTimeSummary)
{

    connect(source, &Context::activityAdded, this, &WTSContext::addActivity);


    for(int i = 0; i < 7; i++)
    {
        mDailyContextSummary.append(new TMTTimeSummary);
    }

    foreach(Activity* activity, source->activities())
    {
        addActivity(activity);
    }

}

QList<QStandardItem *> WTSContext::row()
{
    QList<QStandardItem *>  result;

    result.append(this);
    result.append(mContextWeekTotal);
    foreach(TMTTimeSummary* item, mDailyContextSummary)
        result.append(item);

    return result;

}

void WTSContext::setCurrentDay(JulianDay day)
{
    emit currentDayChanged(day);
}

void WTSContext::addActivity(Activity *activity)
{
    if (activity)
    {
        connect(mSummaryRow, &WTSSummaryRow::tracked, mContextWeekTotal, &TMTTimeSummary::track);
        connect(mSummaryRow, &WTSSummaryRow::trackedAtDay, this, &WTSContext::trackDay);

        WTSActivity* child = new WTSActivity(mSummaryRow, activity);
        connect(this, &WTSContext::currentDayChanged, child, &WTSActivity::setCurrentDay);

        disconnect(mSummaryRow, &WTSSummaryRow::trackedAtDay, this, &WTSContext::trackDay);
        disconnect(mSummaryRow, &WTSSummaryRow::tracked, mContextWeekTotal, &TMTTimeSummary::track);

        appendRow(child->row());


    }

}

void WTSContext::trackDay(int dayInWeek, TMTTimeEntry *entry)
{
    // We steer this programatically, out of bound will clearly crash and imply a bug
    mDailyContextSummary[dayInWeek]->track(entry);
}
